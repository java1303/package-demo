package test;

import geometry.shapes.*;

public class TestRectangle{

public static void main(String[] a){

Rectangle r1,r2,r3;
r1 = new Rectangle(7,5);
r2 = new Rectangle(5);
r3 = null;

Rectangle r4,r5;
Cuboid c1,c2;
r4 = new Rectangle(7,5); 
r5 = new Cuboid(7,5,4);
c1 = new Cuboid(7,5,4);
c2 = new Cuboid(7,5,4);

Square s1;
s1= new Square(4);

System.out.println("\n\nAREA OF r1 IS = " + r1.area());
System.out.println("\nAREA OF r2 IS = " + r2.area());

System.out.println("LENGTH OF r1 IS = " + r1.length);
System.out.println("WIDTH OF r1 IS = " + r1.width);
System.out.println("\nLENGTH OF r2 IS = " + r2.length);
System.out.println("WIDTH OF r2 IS = " + r2.width);

System.out.println("\nSTATUS OF count IS = " + Rectangle.count);
System.out.println("\nr3 = " + r3);


System.out.println("\nAREA OF r4 IS = " + r4.area());
System.out.println("AREA OF r5 IS = " + r5.area());
System.out.println("AREA OF c1 IS = " + c1.area());
System.out.println("\nVOLUME OF c1 IS = " + c1.vol());

System.out.println("\nLENGTH OF r4 IS = " + r4.length);
System.out.println("WIDTH OF r4 IS = " + r4.width);
System.out.println("\nLENGTH OF r5 IS = " + r5.length);
System.out.println("WIDTH OF r5 IS = " + r5.width);
System.out.println("\nLENGTH OF c1 IS = " + c1.length);
System.out.println("WIDTH OF c1 IS = " + c1.width);
System.out.println("HEIGHT OF c1 IS = " + c1.height);
System.out.println("\nLENGTH OF c2 IS = " + c2.length);
System.out.println("WIDTH OF c2 IS = " + c2.width);
System.out.println("HEIGHT OF c2 IS = " + c2.height);

System.out.println("\nLENGTH OF s1 IS = " + s1.length);
System.out.println("WIDTH OF s1 IS = " + s1.width);
}
}