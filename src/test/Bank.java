package test;

import bank.account.*;

public class Bank{
public static void main(String[] a){

SavingsAccount s1,s2;
CurrentAccount c1,c2;

s1 = new SavingsAccount(1001,"PARTH",10000);
s2 = new SavingsAccount("DANTE",5000);

c1 = new CurrentAccount(2001,"BRUCE",3000);
c2 = new CurrentAccount("SAMMY",2000);

System.out.println("\n\n\n**********ALL ACCOUNT DETAILS AT INITIAL STAGE**********\n");
s1.Display();
s2.Display();
c1.Display();
c2.Display();

s1.Deposit(5000);
s2.Deposit(1000);
c1.Deposit(3000);
c2.Deposit(2000);

System.out.println("\n\n\n**********ALL ACCOUNT DETAILS AFTER DEPOSIT**********\n");
s1.Display();
s2.Display();
c1.Display();
c2.Display();

s1.Withdraw(8000);
s2.Withdraw(4000);
c1.Withdraw(8000);
c2.Withdraw(3500);

System.out.println("\n\n\n**********ALL ACCOUNT DETAILS AFTER WITHDRAW**********\n");
s1.Display();
s2.Display();
c1.Display();
c2.Display();
}
}
