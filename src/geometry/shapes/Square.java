package geometry.shapes;

public class Square extends Rectangle{

public Square(int l){
super(l,l);
}

@Override public int area(){
return this.length * this.length;
}
}