package geometry.shapes;

public class Cuboid extends Rectangle{

public int height;

public int vol(){
return super.area() * this.height;
}

public Cuboid(int l,int w,int h){
super(l,w);
this.height = h;
}

@Override public int area(){
return this.length * this.height;
}
}