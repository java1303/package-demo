package geometry.shapes;

public class Rectangle{

public int length,width;
public static int count;

public int area(){
return this.length * this.width;
}

public void SetDimensions(int l,int w){
this.length = l;
this.width = w;
}

public void SetDimensions(int l){
SetDimensions(l,l);
}

public Rectangle(int l,int w){
SetDimensions(l,w);
}

public Rectangle(int l){
this(l,l);
}

public void finalize(){
count--;
}

public static int GetCount(){
return count;
}

{
count++;
}

static{
count=0;
}

}