package bank.account;

public abstract class Account{

public long acno,bal;
public String name;

public Account(long a,String b,long c){
this.acno=a;
this.name=b;
this.bal =c;
}

public static long lastacno=1000;

public Account(String b,long c){
this(++lastacno,b,c);
}

public long GetAcno(){
return this.acno;
}

public String GetName(){
return this.name;
}

public long GetBal(){
return this.bal;
}

public void Deposit(long c){
this.bal += c;
}

public boolean Withdraw(long c){
if(this.bal < c){
System.out.print("\nCANNOT WITHDRAW FROM "+this.acno);
return false;
}
this.bal -= c;
return true;
}

public void Display(){
System.out.println("ACCOUNT NO IS : "+this.acno+" , USER NAME IS : "+this.name+" , BALANCE IS : "+this.bal);
}
}

